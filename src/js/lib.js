'use strict'
const headerBtn = document.querySelector('.burger-btn');
const burgerBtnIcon = document.querySelector('.burger-btn__icon');
const headerList = document.querySelector('.header__list');
const headerItem = document.querySelectorAll('.header__item');
const headerLink = document.querySelectorAll('.header__link');
headerBtn.addEventListener('click', () => {
    burgerBtnIcon.classList.toggle('burger-btn__icon--active');
    headerList.classList.toggle('header__list--active');
    headerItem.forEach(el => {
        el.classList.toggle('header__item--active');
    })
    headerLink.forEach(el => {
        el.classList.toggle('header__link--active');
        
    })
        
})

window.addEventListener('resize', () => {
    burgerBtnIcon.classList.remove('burger-btn__icon--active');
    headerList.classList.remove('header__list--active');
    headerItem.forEach(el => {
        el.classList.remove('header__item--active');
    })
    headerLink.forEach(el => {
        el.classList.remove('header__link--active');
    })
});




